var optdescs = [
	[ 
		0,
		"Play Underground mode to unlock cool rewards"
	],
	[
		1,
		"Test your skill in single player races"
	],
	[
		2,
		"View your career statistics"
	],
	[
		4,
		"Build the ultimate urban exotic"
	],
	[
		5,
		"Tune game settings"
	],
	[
		6,
		"Challenge other players online"
	],
]